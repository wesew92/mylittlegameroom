package org.recreation.mylittlegameroom;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.recreation.mylittlegameroom.model.Gamer;
import org.recreation.mylittlegameroom.service.RegistrationService;
import org.recreation.mylittlegameroom.service.exception.NicknameAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MylittlegameroomApplicationTests {

	@Autowired
	private RegistrationService service;
	
	@Test
	public void test_registrationValidGamer_successful() throws NicknameAlreadyExistsException {
		service.regist(new Gamer("nicky", "12345", "nick@gmail.com", "Nick Doe"));
	}

}
