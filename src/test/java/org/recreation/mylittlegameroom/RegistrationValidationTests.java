package org.recreation.mylittlegameroom;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.recreation.mylittlegameroom.model.Gamer;
import org.recreation.mylittlegameroom.service.RegistrationService;
import org.recreation.mylittlegameroom.service.exception.NicknameAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RegistrationValidationTests {

	@Autowired
	private RegistrationService service;
	
	@Test(expected = ConstraintViolationException.class)
	public void test_registrationWithNullGamer_fails() throws NicknameAlreadyExistsException {
		service.regist(null);
	}
	
	@Test(expected = ConstraintViolationException.class)
	public void test_registrationWithNameOfSpaceOnly_fails() throws NicknameAlreadyExistsException {
		service.regist(new Gamer("  ", "Nick Doe", "12345", "nick@awesome.com"));
	}
	
	@Test(expected = ConstraintViolationException.class)
	public void test_registrationWithInvalidEmailAddressFormat_fails() throws NicknameAlreadyExistsException {
		service.regist(new Gamer("nicky", "Nick Doe", "12345", "nicksome.com"));
	}

}
