package org.recreation.mylittlegameroom.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.recreation.mylittlegameroom.model.Gamer;
import org.recreation.mylittlegameroom.service.RegistrationService;
import org.recreation.mylittlegameroom.service.exception.NicknameAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@RunWith(SpringRunner.class)
@WebMvcTest(GameroomIndexController.class)
public class GameroomIndexControllerTest {

	private static final String PATH_INDEX_FORM = "/gameroom_index";
	private static final String PATH_SUCCESSFUL_REGISTRATION_PAGE = "/successful_registration";
	
	private static final String FORM_FIELD_NICKNAME = "nickName";
	private static final String FORM_FIELD_PASSWORD = "password";
	private static final String FORM_FIELD_EMAIL = "email";
	private static final String FORM_FIELD_REALNAME = "realName";
	
	private static final String VIEW_INDEX_PAGE = "gameroom_index";
	private static final String VIEW_SUCCESSFUL_REGISTRATION_PAGE = "successful_registration";
	
	private static final String CONTENTTYPE_HTML_UTF8 = "text/html;charset=UTF-8";
	
	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private RegistrationService registrationService;
	
	private ResultActions result;
	
	private void given_theUserIsOnTheIndexPage() throws Exception{
		result = mvc.perform(
				get(PATH_INDEX_FORM)
				.accept(MediaType.TEXT_HTML)
				.locale(Locale.ENGLISH)
				)
				.andDo(print())
				.andExpect(content().contentType(CONTENTTYPE_HTML_UTF8))
				.andExpect(status().isOk())
				.andExpect(view().name(VIEW_INDEX_PAGE));
	}
	
	private void then_theFormContains(final Gamer gamer) throws Exception{
		result.andExpect(xpath("//input[@name='" + FORM_FIELD_NICKNAME + "']/@value").string(gamer.getNickName()))
		.andExpect(xpath("//input[@name='" + FORM_FIELD_PASSWORD + "']/@value").string(gamer.getPassword()))
		.andExpect(xpath("//input[@name='" + FORM_FIELD_EMAIL + "']/@value").string(gamer.getEmail()))
		.andExpect(xpath("//input[@name='" + FORM_FIELD_REALNAME + "']/@value").string(gamer.getRealName()))
		;
	}
	
	@Test
	public void testRegistrationFormIsEmptyWhenRegistrationFormIsOpened() throws Exception{
		given_theUserIsOnTheIndexPage();
		then_theFormContains(new Gamer("", "", "", ""));
		then_registrationIsNotSentToTheService();
	}

	private void when_userSubmitsRegistrationFormContaining(Gamer gamerFormInput) throws Exception {
		result = mvc.perform(
				post(PATH_INDEX_FORM)
				.accept(MediaType.TEXT_HTML)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.param(FORM_FIELD_NICKNAME, gamerFormInput.getNickName())
				.param(FORM_FIELD_PASSWORD, gamerFormInput.getPassword())
				.param(FORM_FIELD_EMAIL, gamerFormInput.getEmail())
				.param(FORM_FIELD_REALNAME, gamerFormInput.getRealName())
				);
	}
	
	private void then_theUserIsRedirectedToTheSuccessfulRegistrationPage() throws Exception {
		result.andDo(print())
		  .andExpect(status().is3xxRedirection())
		  .andExpect(redirectedUrl(PATH_SUCCESSFUL_REGISTRATION_PAGE));
	}
	
	@Test
	public void testRegistrationFormTakesCorrectlySubmittedData() throws Exception {
		given_theUserIsOnTheIndexPage();
		Gamer validGamer = new Gamer("nicky", "12345", "nicky@gmail.com", "Nick Nolte");
		when_userSubmitsRegistrationFormContaining(validGamer);
		then_registrationIsSentToTheService(validGamer);
		then_theUserIsRedirectedToTheSuccessfulRegistrationPage();
	}
	
	private void then_theRegistrationIsRefusedDueToBadRequest() throws Exception {
		result.andDo(print())
		      .andExpect(status().isBadRequest())
		      .andExpect(xpath("//div[@class='formErrormessage']").exists());
	}
	
	private void then_fieldErrorDisplayedFor(String fieldName) throws Exception {
		result.andExpect(xpath("//div[@class='fieldErrormessage']/following-sibling::*[position()=1][@name'" + fieldName + "']").exists());
	}
	
	private void then_fieldErrorNotDisplayedFor(String fieldName) throws Exception {
		result.andExpect(xpath("//div[@class='fieldErrormessage']/following-sibling::*[position()=1][@name'" + fieldName + "']").doesNotExist());
	}
	

	@Test
	public void testRegistrationFormRefusesInputWithMissingNickname() throws Exception {
		given_theUserIsOnTheIndexPage();
		
		final Gamer gamerWithMissingNickname = new Gamer("", "12345", "nicky@gmail.com", "Nick Nolte");
		when_userSubmitsRegistrationFormContaining(gamerWithMissingNickname);
		
		then_theRegistrationIsRefusedDueToBadRequest();
		then_fieldErrorDisplayedFor(FORM_FIELD_NICKNAME);
		then_fieldErrorNotDisplayedFor(FORM_FIELD_PASSWORD);
		then_fieldErrorNotDisplayedFor(FORM_FIELD_EMAIL);
		then_fieldErrorNotDisplayedFor(FORM_FIELD_REALNAME);
		then_theFormContains(gamerWithMissingNickname);
		then_registrationIsNotSentToTheService();
	}
	
	
	private void then_registrationIsSentToTheService(final Gamer gamer) throws NicknameAlreadyExistsException {
		verify(registrationService, times(1)).regist(gamer);
	}
	
	private void then_registrationIsNotSentToTheService() throws NicknameAlreadyExistsException {
		verify(registrationService, times(0)).regist(any());
	}
}
