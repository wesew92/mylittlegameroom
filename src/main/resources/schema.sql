CREATE TABLE IF NOT EXISTS gamers (
	id VARCHAR(36) PRIMARY KEY,
	nickname VARCHAR(30) NOT NULL,
	password VARCHAR(64) NOT NULL,
	email VARCHAR(64) NOT NULL,
	realname VARCHAR(36),
	UNIQUE KEY UK_nickname (nickname)
);