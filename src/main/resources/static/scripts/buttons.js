$(document).ready(function(){
	var activeLogin = false;
	var activeReg = false;

    $("#login-button").click(function(){
        if(!activeReg){
            activeLogin = !activeLogin;
        	$("#loginform").slideToggle(400);
        }
    });

    $("#reg-button").click(function(){
        if(!activeLogin){
        	activeReg = !activeReg;
        	$("#registrationform").slideToggle(400);
        }
    });

});