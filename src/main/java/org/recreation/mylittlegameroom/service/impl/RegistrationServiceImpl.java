package org.recreation.mylittlegameroom.service.impl;

import org.recreation.mylittlegameroom.model.Gamer;
import org.recreation.mylittlegameroom.repository.RegistrationRepository;
import org.recreation.mylittlegameroom.service.RegistrationService;
import org.recreation.mylittlegameroom.service.exception.NicknameAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RegistrationServiceImpl implements RegistrationService {

	@Autowired
	private RegistrationRepository repository;

	@Override
	@Transactional(rollbackFor = NicknameAlreadyExistsException.class)
	public void regist(Gamer gamer) throws NicknameAlreadyExistsException {
		
		try {
			repository.save(gamer);
		} catch (DuplicateKeyException duplicate) {
			throw new NicknameAlreadyExistsException(
					"Nickname already exsists with thsi nickname: " + gamer.getNickName());
		}
	}

}
