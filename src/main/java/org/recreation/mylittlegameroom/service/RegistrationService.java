package org.recreation.mylittlegameroom.service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.recreation.mylittlegameroom.model.Gamer;
import org.recreation.mylittlegameroom.service.exception.NicknameAlreadyExistsException;
import org.springframework.validation.annotation.Validated;

@Validated
public interface RegistrationService {

	void regist(@NotNull @Valid Gamer gamer) throws NicknameAlreadyExistsException;
}
