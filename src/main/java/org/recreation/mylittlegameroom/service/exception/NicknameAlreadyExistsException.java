package org.recreation.mylittlegameroom.service.exception;

public class NicknameAlreadyExistsException extends Exception {

	private static final long serialVersionUID = -5556380677265056298L;

	public NicknameAlreadyExistsException(String message) {
		super(message);
	}

}
