package org.recreation.mylittlegameroom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MylittlegameroomApplication{
	
	public static void main(String[] args) {
		SpringApplication.run(MylittlegameroomApplication.class, args);
	}

}
