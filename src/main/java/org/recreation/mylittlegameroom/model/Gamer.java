package org.recreation.mylittlegameroom.model;

import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


public class Gamer {

	@NotBlank
	@Size(min=3, max=30)
	private String nickName;

	@NotBlank
	@Size(min=3, max=30)
	private String password;

	@NotBlank
	@Email
	private String email;

	@Size(min=0, max=40)
	private String realName;
	
	private List<Gamer> friendList;
	
	public Gamer() {
		
	}
	
	public Gamer(@NotBlank @Size(min = 3, max = 30) String nickName,
			@NotBlank @Size(min = 3, max = 30) String password) {
		this.nickName = nickName;
		this.password = password;
	}



	public Gamer(@NotBlank @Size(min = 3, max = 30) String nickName, @NotBlank @Size(min = 3, max = 30) String password,
			@NotBlank @Email String email) {
		this.nickName = nickName;
		this.password = password;
		this.email = email;
	}

	public Gamer(@NotBlank @Size(min = 3, max = 30) String nickName, @NotBlank @Size(min = 3, max = 30) String password,
			@NotBlank @Email String email, @Size(min = 0, max = 40) String realName) {
		this.nickName = nickName;
		this.password = password;
		this.email = email;
		this.realName = realName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public List<Gamer> getFriendList() {
		return friendList;
	}

	public void setFriendList(List<Gamer> friendList) {
		this.friendList = friendList;
	}

	@Override
	public String toString() {
		return "Gamer [nickName=" + nickName + ", password=" + password + ", email=" + email + ", realName=" + realName
				+ ", friendList=" + friendList + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((friendList == null) ? 0 : friendList.hashCode());
		result = prime * result + ((nickName == null) ? 0 : nickName.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((realName == null) ? 0 : realName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gamer other = (Gamer) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (friendList == null) {
			if (other.friendList != null)
				return false;
		} else if (!friendList.equals(other.friendList))
			return false;
		if (nickName == null) {
			if (other.nickName != null)
				return false;
		} else if (!nickName.equals(other.nickName))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (realName == null) {
			if (other.realName != null)
				return false;
		} else if (!realName.equals(other.realName))
			return false;
		return true;
	}
}
