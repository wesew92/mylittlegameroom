package org.recreation.mylittlegameroom.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.recreation.mylittlegameroom.model.Gamer;
import org.recreation.mylittlegameroom.service.RegistrationService;
import org.recreation.mylittlegameroom.service.exception.NicknameAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class GameroomIndexController {

	private static final String GAMEROOM_INDEX_FORM = "gameroom_index";
	private static final String REGISTRATION_FORM_ATTRIBUTE = "registrationForm";
	private static final String LOGIN_FORM_ATTRIBUTE = "loginForm";
	
	@Autowired
	private RegistrationService registrationService;

	@GetMapping(value= {"/", "/index", "/gameroom_index"})
	public String getGameroomIndexForm(@ModelAttribute(REGISTRATION_FORM_ATTRIBUTE) Gamer regGamer, @ModelAttribute(LOGIN_FORM_ATTRIBUTE) Gamer logGamer) {
		return GAMEROOM_INDEX_FORM;
	}
	
	@PostMapping("/registration")
	public String submitRegistration(@ModelAttribute(REGISTRATION_FORM_ATTRIBUTE) @Valid Gamer regGamer, 
			BindingResult result, HttpServletResponse response) {
		
		String targetView = GAMEROOM_INDEX_FORM;
		
		if(result.hasErrors()) {
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			result.reject("registrationForm.error.incompleteInput");
		}else {
			try {
				registrationService.regist(regGamer);
				targetView = "redirect:/successful_registration";
			} catch (NicknameAlreadyExistsException e) {
				result.reject("error.userError.nickname.already.exists");
			}
			
		}
		return targetView;
	}
	
	@PostMapping("/login")
	public String submitLogin() {
		return "redirect:/games_main_page";
	}

	
//	@PostMapping("/login")
//	public String submitLogin(@ModelAttribute(LOGIN_FORM_ATTRIBUTE) @Valid Gamer logGamer, 
//			BindingResult result, HttpServletResponse response) {
//		
//		if(result.hasErrors()) {
//			response.setStatus(HttpStatus.BAD_REQUEST.value());
//			result.reject("loginForm.error.incompleteInput");
//			return GAMEROOM_INDEX_FORM;
//		}else {
//			return "redirect:/games_main_page";
//		}
//	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.CONFLICT)
	public String nicknameAleradyExists(NicknameAlreadyExistsException e) {
		return "error/error-nickname-already-exists";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		StringTrimmerEditor trimer = new StringTrimmerEditor(false);
		binder.registerCustomEditor(String.class, trimer);
	}
}
