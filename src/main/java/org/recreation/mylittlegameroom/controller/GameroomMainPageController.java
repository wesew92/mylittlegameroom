package org.recreation.mylittlegameroom.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class GameroomMainPageController {
	
	@GetMapping("/games_main_page")
	public String getGameroomMainPage() {
		return "games_main_page";
	}

}
