package org.recreation.mylittlegameroom.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SuccessfulRegistrationController {

	@GetMapping("/successful_registration")
	public String getSuccessfulRegistrationPage() {
		return "successful_registration";
	}
}
