package org.recreation.mylittlegameroom.repository;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.recreation.mylittlegameroom.model.Gamer;
import org.springframework.validation.annotation.Validated;

@Validated
public interface RegistrationRepository {

	void save(@NotNull @Valid Gamer gamer);
}
