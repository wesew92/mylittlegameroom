package org.recreation.mylittlegameroom.repository.impl;

import java.util.UUID;

import org.recreation.mylittlegameroom.model.Gamer;
import org.recreation.mylittlegameroom.repository.RegistrationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class RegistrationRepositoryImpl implements RegistrationRepository {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public void save(Gamer gamer) {
		logger.info(" 'saves' {} now... " + gamer);

		final String sql = "INSERT INTO gamers (id, nickname, password, email, realname) VALUES (?,?,?,?,?)";
		final String id = UUID.randomUUID().toString();
		jdbcTemplate.update(sql, id, gamer.getNickName(), gamer.getPassword(), gamer.getEmail(), gamer.getRealName());
	}

}
